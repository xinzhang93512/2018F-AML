# CS980: Advanced Machine Learning #


### Where and When ###

- Kingsbury N233
- MW  3:40pm - 5:00pm

# What #

This seminar will cover *reinforcement learning*. The goal in reinforcement learning is to learn how to act while interacting with a dynamic and complex environment. Instead of trying to cover all reinforcement learning topics, we will focus on the foundations needed to understand other concepts.

1. Markov decision processes: Policy iteration, Value iteration, Linear programming 
2. Value Function Approximation: LSTD, LSPI, ALP, ABP
3. Uncertainty: Bandits and Robust Markov Decision Processes

We will use **Python** (and a little bit of R and C++) and cover relevant topics from linear algebra, mathematical optimization, and statistics as needed.

# Schedule

The schedule is preliminary. We will adapt it based on our progress and interests.

| Date  | Day     | Topic/Slides                                                            | Reading       | Presenter | Notes
| ----- | ------- | :-----------------------------:                                         | ------------- | --------- | -----
|Aug 27 | Mon     | [Introduction to Reinforcement Learning](slides/class1/class1.pdf)      |               | Marek     |
|Aug 29 | Wed     | Newsvendor problem: Data-driven decision making (Project)               | NW            | Marek     | [class1](notes/class1.md) |
|Sep 03 | Mon     | *Memorial Day*                                                          |               |           |
|Sep 05 | Wed     | Model Formulation / project discussion                                  | MDP 2,3       | Marek     | [class2](notes/class2.md)
|Sep 10 | Mon     | Policy evaluation                                                       | MDP 4,5       | Marek     | [class3](notes/class3.md) [assignment](assignments/assignment1.md)
|Sep 12 | Wed     | Value iteration implementation                                          | MDP 6.3       | G1        |
|Sep 17 | Mon     | Policy iteration implementation                                         | MDP 6.4       | G2        |
|Sep 19 | Wed     | Modified policy iteration                                               | MDP 6.5       | G3        |
|Sep 24 | Mon     | Linear programs for MDPs                                                | MDP 6.9       | G1,G2,G3  |
|Sep 26 | Wed     | Tax Collection Optimization                                             | *paper*       | Yi        |
|Oct 01 | Mon     | Solving Go                                                              | *paper*       | Sai       |
|Oct 03 | Wed     | Optimizers Curse                                                        | *paper*       | Kaixin    |
|Oct 09 | Tue     | Least squares temporal difference                                       | *paper*       | Alison    |
|Oct 10 | Wed     | LSTD implementation                                                     |               | G1        |
|Oct 15 | Mon     | Least squares policy iteration                                          | *paper*       | Ram       |
|Oct 17 | Wed     | LSPI implementation                                                     |               | G2        |
|Oct 22 | Mon     | Approximate linear programming                                          | *paper*       | Nick      |
|Oct 24 | Wed     | ALP implementation                                                      |               | G3        |
|Oct 29 | Mon     | Robust MDPs Intro                                                       |               | Marek     |
|Oct 31 | Wed     | Bayesian Inference                                                      | ALML, pg 41   | Student   |
|Nov 05 | Mon     | Concentration inequalities                                              | ALML, pg 208  | Marek     |
|Nov 07 | Wed     | Model-based Interval Estimation for MDPs                                | *paper*       | Soheil    |
|Nov 12 | Mon     | *Veteran's Day*                                                         |               |           |
|Nov 14 | Wed     | PAC Optimal MDP Planning                                                | *paper*       | Shayan    |
|Nov 19 | Mon     | Safe Policy Improvement                                                 | *paper*       | Peihao    |
|Nov 21 | Wed     | *Thanksgiving*                                                          |               |           |
|Nov 26 | Mon     | Performance loss bounds                                                 | *paper*       | Marek     |
|Nov 28 | Wed     | Policy gradient methods                                                 | *paper*       | Amith     |
|Dec 03 | Mon     | Distributional Bellman Operator                                         | *paper*       | Xin       |
|Dec 05 | Wed     | Project Presentations and Discussion                                    |               | All       |
|Dec 10 | Mon     | Project Presentations and Discussion                                    |               | All       |


The project and implementations will be done in 3 groups: G1, G2, G3

## Grading ##

- Paper presentation  (10%)
- Paper response and a mini-quiz (20%)
- Implementation problems (30%)
- Project (40%)

## Presenting a Paper ##

Every student will be expected to present at least one paper and lead the discussion about the paper. Please review the following helpful guide on how to review read research papers. [How to read a research paper](docs/efficientReading.pdf)

When presenting a paper, your presentation should answer the following points:

1. *Motivation*: Why is this problem important?
-  *Challenge*: Why is it difficult?
-  *Approach*: What is the main approach?
-  *Novelty*: What is new about the paper?
-  *Evidence*: Is there sufficient evidence of the effectiveness of the methods?
-  *Limitations*: What is it that the paper does not address? Are there alternative methods that can work?

Please prepare slides for the presentation that include relevant figures or tables from the paper.

The presentation should take about 1h.

#### Paper Assignments

|Paper                                    |Presenter |
|:----------------------------------------|:---------|
|Tax Collection Optimization              |Yi        |
|Solving Go                               |Sai       |
|Optimizer's curse                        |Kaixin    |
|Least Squares Temporal Difference        |Alison    |
|Least-Squares Policy Iteration           |Ram       |
|Approximate Linear Programming           |Nick      |
|Model-based Interval Estimation for MDPs |Soheil    |
|PAC Optimal MDP Planning                 |Shayan    |
|Safe Policy Improvement                  |Peihao    |
|Performance Loss Bounds                  |Marek     |
|Policy Gradient Methods                  |Amith     |
|Distributional Bellman Operator          |Xin       |

This assignment maximizes the total [preferences](https://docs.google.com/spreadsheets/d/1Ba6MeCUOgFwt73rk-1qiXSUOWwoAr6ndb67evq5A_ZM/edit#gid=0) (and almost everyone gets their top choice). The code that uses MILP to find the best paper to student match is [here](code/papers/optimize.R).


## Writing a Paper Response ##

For every paper (column reading says: "paper"), every student is expected to write a short paper response. The response should consist of:

1. A paragraph describing the main idea of the paper
2. 1-3 paragraphs addressing the points mentioned in the section "Presenting a Paper"

## Implementation Problems ##

Each person should complete 3 implementation challenges as a part of the group. The group should implement the assigned algorithm and present the implementation in the class. The implementation should focus on correctness and conciseness. Each assignment should be completed in less than 500 lines and probably possible with less than 100.

More details are available [here](solutions/mdp_implementation.md)

## Project ##

The project will involve designing a reinforcement learning algorithm for managing invasive species. We have a realistic simulator available for glossy buckthorn, a shrub invasive in North America. We will design, implement, and evaluate new algorithms for solving the problem together.

# Sources #

## Books ##

### MDPs ###
- [MDP]: Puterman, M. L. (2005). Markov decision processes: Discrete stochastic dynamic programming. John Wiley & Sons, Inc.

### RL ###
- [ARL]: Szepesvári, C. (2010). [Algorithms for Reinforcement Learning](https://sites.ualberta.ca/~szepesva/RLBook.html). 
- [RL]: Sutton, R. S., & Barto, A. (1998). [Reinforcement learning](http://incompleteideas.net/book/the-book-2nd.html). MIT Press.

### Probability and Statistics ###
- Grinstead, C., & Snell, J. (1998). [Introduction to probability](https://www.dartmouth.edu/~chance/teaching_aids/books_articles/probability_book/amsbook.mac.pdf). 
- Lavine, M. (2005). [Introduction to statistical thought](http://people.math.umass.edu/~lavine/Book/book.html).

### Advanced ML ###
- [ALML]: [Advanced Lectures on Machine Learning](https://link.springer.com/book/10.1007/b100712)

### Other ###
- [NW]: [Newsvendor Notes](http://courses.ieor.berkeley.edu/ieor151/lecture_notes/ieor151_lec5.pdf)

## Paper Links ##

- [Tax Collection Optimization](https://pubsonline.informs.org/doi/abs/10.1287/inte.1110.0618)
- [Solving Go](https://www.nature.com/articles/nature24270)
- [Distributional Bellman Operator](https://arxiv.org/pdf/1707.06887.pdf)
- [Model-based Interval Estimation for MDPs](https://www.sciencedirect.com/science/article/pii/S0022000008000767)
- [PAC Optimal MDP Planning](http://jmlr.org/papers/v16/taleghan15a.html)
- [Safe Policy Improvement by Minimizing Robust Baseline Regret](http://marek.petrik.us/pub/Petrik2016b.pdf)
- [Perormance Loss Bounds](https://pubsonline.informs.org/doi/pdf/10.1287/moor.1060.0188)
- [Least Squares Temporal Difference](https://link.springer.com/article/10.1007/BF00114723)
- [Approximate Linear Programming](http://www.mit.edu/~pucci/discountedLP.pdf)
- [Least-Squares Policy Iteration](http://jmlr.csail.mit.edu/papers/v4/lagoudakis03a.html)
- [Optimizer's curse](https://faculty.fuqua.duke.edu/~jes9/bio/The_Optimizers_Curse.pdf)
- [Policy Gradient Methods](https://papers.nips.cc/paper/1713-policy-gradient-methods-for-reinforcement-learning-with-function-approximation.pdf)
