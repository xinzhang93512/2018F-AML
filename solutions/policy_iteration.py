import numpy as np

if __name__ == "__main__":

    # Discount
    gamma = 1.02

    # Transition probabilities
    transition = np.array([[0.8, 0.2],
                           [0.4, 0.6]])

    reward = [[200, -20],  # Keep
              [5090, 5090]]  # Sell

    # Select an arbitrary policy
    KEEP = 0
    SELL = 1
    p_0 = [None, None]
    p_1 = [KEEP, SELL]

    while (p_1 != p_0):
        p_0 = p_1
        for i, state in enumerate(p_0):
            # Evaluate the current policy
            v = np.linalg.solve(np.eye(2) - gamma * transition, reward[state])
            # Try to improve it
            p_1[i] = np.argmax([
                reward[KEEP][state] + gamma * np.sum(transition[state] * v[state]),
                reward[SELL][state] + gamma * np.sum(transition[state] * v[state])])
            print(transition[state] * v[state])
    print(p_1)